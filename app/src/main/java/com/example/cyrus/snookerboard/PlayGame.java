package com.example.cyrus.snookerboard;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class PlayGame extends AppCompatActivity
{
    private ArrayList<GameStatus> GameStatusHolder;

    private String Player1ID,Player2ID;
	Player Player1() {return PSave.Players.get(Player1ID);}
	Player Player2() {return PSave.Players.get(Player2ID);}

    private int Player1Score, Player2Score;
    private int Player1Frames, Player2Frames;

    private HashMap<Balls, Integer> BallsOnTable;

    private ArrayList<Frame> FrameVector;
    private Break CurrentBreak;

    private int CurrentBall;
    //private int CurrentBreakVal;

    private Boolean Player1Turn, RedPotted, Player2First;

    private LinearLayout BreakBallHolder;

	private int CurrentTurn;

////////////////////////////////////////////////////////////////////////////////////////////////////
    private TextView txt_Player1Name;
    private TextView txt_Player2Name;
    private TextView txt_Player1Score;
    private TextView txt_Player2Score;
    private TextView txt_BreakCounter;
    private TextView txt_Player1Ahead;
    private TextView txt_Player2Ahead;
    private TextView txt_Player1FrameCount;
    private TextView txt_Player2FrameCount;
    private TextView txt_PointsRemaining;

    private View Player1Triangle;
    private View Player2Triangle;

    private Button btn_RedBall;
    private Button btn_YellowBall;
    private Button btn_GreenBall;
    private Button btn_BrownBall;
    private Button btn_BlueBall;
    private Button btn_PinkBall;
    private Button btn_BlackBall;
    private ImageButton btn_Undo;
    private ImageButton btn_Foul;
    private Button btn_FinishFrame;
    private ImageButton btn_EndBreak;
    private ImageButton btn_WhiteFlag;

	private Long FrameID;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_game);

	    int FrameCount = 0;

        Bundle Extras = getIntent().getExtras(); // gets the setup variables
	    if (Extras != null)
	    {
		    FrameCount = Extras.getInt("FrameCount");
            Player1ID = Extras.get("Player1ID").toString(); //Get the players from the hashmap
            Player2ID = Extras.get("Player2ID").toString();
	    }

        txt_Player1Name = (TextView)findViewById(R.id.txt_Player1Name);
	    if (!Player1().GetSecondName().equals(""))
		    txt_Player1Name.setText(Player1().GetFirstName() + "\n" + Player1().GetSecondName());
	    else
		    txt_Player1Name.setText(Player1().GetFirstName());

        txt_Player2Name = (TextView)findViewById(R.id.txt_Player2Name);
	    if (!Player2().GetSecondName().equals(""))
		    txt_Player2Name.setText(Player2().GetFirstName() + "\n" + Player2().GetSecondName());
	    else
		    txt_Player2Name.setText(Player2().GetFirstName());

        txt_BreakCounter = (TextView)findViewById(R.id.txt_BreakCounter);
        txt_BreakCounter.setText("0");

        txt_Player1Score = (TextView)findViewById(R.id.txt_Player1Score);
        txt_Player2Score = (TextView)findViewById(R.id.txt_Player2Score);

        txt_Player1Ahead = (TextView)findViewById(R.id.txt_Player1Ahead);
        txt_Player2Ahead = (TextView)findViewById(R.id.txt_Player2Ahead);

        txt_Player1FrameCount = (TextView)findViewById(R.id.txt_Player1FrameCount);
        txt_Player2FrameCount = (TextView)findViewById(R.id.txt_Player2FrameCount);

        txt_PointsRemaining = (TextView)findViewById(R.id.txt_PointsRemaining);
        txt_PointsRemaining.setText("147 Points Remaining");

	    TextView txt_FrameCount = (TextView)findViewById(R.id.txt_FrameCount);
	    txt_FrameCount.setText("[" + FrameCount + "]"); // sets frame counter

        btn_RedBall = (Button)findViewById(R.id.btn_RedBall);
        btn_RedBall.setOnClickListener(onClickListener);
        //Long press on red ball listener
        btn_RedBall.setOnLongClickListener(new View.OnLongClickListener()
        {
	        @Override
	        public boolean onLongClick(View v)
	        {
		        CreateMultiRedDialog();
		        return false;
	        }
        });
        btn_RedBall.setBackgroundResource(R.drawable.red);

        btn_YellowBall = (Button)findViewById(R.id.btn_YellowBall);
        btn_YellowBall.setOnClickListener(onClickListener);
        btn_YellowBall.setBackgroundResource(R.drawable.yellow);

        btn_GreenBall = (Button)findViewById(R.id.btn_GreenBall);
        btn_GreenBall.setOnClickListener(onClickListener);
        btn_GreenBall.setBackgroundResource(R.drawable.green);

        btn_BrownBall = (Button)findViewById(R.id.btn_BrownBall);
        btn_BrownBall.setOnClickListener(onClickListener);
        btn_BrownBall.setBackgroundResource(R.drawable.brown);

        btn_BlueBall = (Button)findViewById(R.id.btn_BlueBall);
        btn_BlueBall.setOnClickListener(onClickListener);
        btn_BlueBall.setBackgroundResource(R.drawable.blue);

        btn_PinkBall = (Button)findViewById(R.id.btn_PinkBall);
        btn_PinkBall.setOnClickListener(onClickListener);
        btn_PinkBall.setBackgroundResource(R.drawable.pink);

        btn_BlackBall = (Button)findViewById(R.id.btn_BlackBall);
        btn_BlackBall.setOnClickListener(onClickListener);
        btn_BlackBall.setBackgroundResource(R.drawable.black);

        btn_Undo = (ImageButton)findViewById(R.id.btn_Undo);
        btn_Undo.setOnClickListener(onClickListener);

        btn_FinishFrame = (Button)findViewById(R.id.btn_FinishFrame);
        btn_FinishFrame.setOnClickListener(onClickListener);

        btn_Foul = (ImageButton)findViewById(R.id.btn_Foul);
        btn_Foul.setOnClickListener(onClickListener);
        //
        btn_Foul.setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View v)
            {
                CreateFreeBallDialog();
                return false;
            }
        });

        btn_EndBreak = (ImageButton)findViewById(R.id.btn_EndBreak);
        btn_EndBreak.setOnClickListener(onClickListener);

        btn_WhiteFlag =  (ImageButton)findViewById(R.id.btn_WhiteFlag);
        btn_WhiteFlag.setOnClickListener(onClickListener);

	    Player1Triangle = (View)findViewById(R.id.Player1Triangle);
	    Player2Triangle = (View)findViewById(R.id.Player2Triangle);

        BreakBallHolder = (LinearLayout)findViewById(R.id.BreakBallHolder);

        BallsOnTable = new HashMap<>();
        //CurrentBreak = new Break();
	    FrameVector = new ArrayList<>();
        GameStatusHolder = new ArrayList<>();

        Player2First = false;

	    FrameID = System.currentTimeMillis();

	    StartNewFrame();

        UpdateDisplays();
    }
////////////////////////////////////////////////////////////////////////////////////////////////////

    private void StartNewFrame()
    {
        Player2First = !Player2First;
        Player1Turn = Player2First;

        BallsOnTable.put(Balls.Red, 15); // adds the balls and counts onto the table
        BallsOnTable.put(Balls.Yellow, 1);
        BallsOnTable.put(Balls.Green, 1);
        BallsOnTable.put(Balls.Brown, 1);
        BallsOnTable.put(Balls.Blue, 1);
        BallsOnTable.put(Balls.Pink, 1);
        BallsOnTable.put(Balls.Black, 1);

        Player1Score = 0;
        Player2Score = 0;

        CurrentBall = 1;
        //
        CurrentBreak = new Break(GetCurrentPlayerID());
        RedPotted = false;
        BreakBallHolder.removeAllViews();

	    FrameVector.add(new Frame());

        //SaveGameState();

        UpdateButtons();
        UpdateDisplays();
    }

    private void PotBall(Balls Ball, boolean Save) // Pass in player and ball
    {
        if (Save)
            SaveGameState();

	    Shot NewShot;
        NewShot = new Shot(GetCurrentPlayerID());
	    NewShot.PotBall(Ball);

        CurrentBreak.AddShot(NewShot);

        AddBall(Ball);

        //If any reds remain then change ball when a red is potted
        if (CurrentBall == 1) // and return it when a color is
        {
            RedPotted = !RedPotted;
        }

        //Handles removing the ball from the list

        if (CurrentBall == Ball.GetVal()) // Checks that the potted ball is the current target ball
        {
            BallsOnTable.put(Ball, BallsOnTable.get(Ball) - 1); // Reduces the count of the ball by 1
        }

        //Handles giving the correct player the points
        if (Player1Turn)
        {
            Player1Score += Ball.GetVal();
        }
        else
        {
            Player2Score += Ball.GetVal();
        }

        // Check that the potted ball changes the currentball
        if ((!RedPotted) & (BallsOnTable.get(Balls.Red) == 0))
        {
            CurrentBall++;
        }

	    EndTurn();
    }

    private View.OnClickListener onClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            String Winner = "";
            switch(v.getId())
            {
                case R.id.btn_RedBall:
                    PotBall(Balls.Red, true);
                    break;
                case R.id.btn_YellowBall:
                    PotBall(Balls.Yellow, true);
                    break;
                case R.id.btn_GreenBall:
                    PotBall(Balls.Green, true);
                    break;
                case R.id.btn_BrownBall:
                    PotBall(Balls.Brown, true);
                    break;
                case R.id.btn_BlueBall:
                    PotBall(Balls.Blue, true);
                    break;
                case R.id.btn_PinkBall:
                    PotBall(Balls.Pink, true);
                    break;
                case R.id.btn_BlackBall:
                    PotBall(Balls.Black, true);
                    break;
                case R.id.btn_EndBreak:

                    SaveGameState();

	                //Adds miss to break
	                Shot NewShot;
	                NewShot = new Shot(GetCurrentPlayerID());

	                CurrentBreak.AddShot(NewShot);

                    Player1Turn = !Player1Turn;

                    if (( BallsOnTable.get(Balls.Red) == 0 )&( CurrentBall == 1 ))
                    {
                        CurrentBall++;
                    }

                    AddEndLine();

	                FrameVector.get(FrameVector.size() - 1).AddBreak(new Break(CurrentBreak)); //adds break to end of latest frame

	                CurrentBreak.GetShots().clear();
                    CurrentBreak = new Break(GetCurrentPlayerID());
                    RedPotted = false;
                    //BreakBallHolder.removeAllViews();

                    EndTurn();

                    break;
                case R.id.btn_Foul:

                    if (Player1Turn)
                        CreateFoulDialog(true);
                    else
                        CreateFoulDialog(false);

                    break;
                case R.id.btn_Undo:
                    //Revert gamestate to previous one
                    RevertGameState();

                    UpdateDisplays();
                    UpdateButtons();

                    break;
                case R.id.btn_WhiteFlag:

                    SaveGameState();

                    if (Player1Turn) // Player 1 resigns
                    {
                        Player2Frames++;
                        Winner = Player2ID;
                    }
                    else // Player 2 resigns
                    {
                        Player1Frames++;
                        Winner = Player1ID;
                    }

	                FrameVector.get(FrameVector.size() - 1).AddBreak(new Break(CurrentBreak)); //adds break to end of latest frame

	                Player1().ProcessFrame(new ArrayList<>(FrameVector.get(FrameVector.size() - 1).GetBreakVector()), Winner);
	                Player2().ProcessFrame(new ArrayList<>(FrameVector.get(FrameVector.size() - 1).GetBreakVector()), Winner);
	                PSave.ReplacePlayers(PlayGame.this);
	                StartNewFrame();

                    break;
                case R.id.btn_FinishFrame:

                    SaveGameState();

                    btn_FinishFrame.setVisibility(View.INVISIBLE);

                    if (Player1Score > Player2Score)
                    {
                        Player1Frames++;
                        Winner = Player1ID;
                    }
                    else if (Player2Score > Player1Score)
                    {
                        Player2Frames++;
                        Winner = Player2ID;
                    }

	                FrameVector.get(FrameVector.size() - 1).AddBreak(new Break(CurrentBreak)); //adds break to end of latest frame

	                Player1().ProcessFrame(new ArrayList<>(FrameVector.get(FrameVector.size() - 1).GetBreakVector()), Winner);
	                Player2().ProcessFrame(new ArrayList<>(FrameVector.get(FrameVector.size() - 1).GetBreakVector()), Winner);
	                PSave.ReplacePlayers(PlayGame.this);

	                CurrentBreak.GetShots().clear();
	                CurrentBreak = new Break(GetCurrentPlayerID());
	                StartNewFrame();
                    //SaveGameState();

                    break;
            }
        }
    };

    private void CreateFoulDialog(final boolean Player1Foul)
    {
        // custom dialog
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_play_frame_foul);
        if (Player1Foul)
            dialog.setTitle("Foul by " + Player1().GetFirstName() + " " + Player1().GetSecondName());
        else
            dialog.setTitle("Foul by " + Player2().GetFirstName() + " " + Player2().GetSecondName());

        final NumberPicker spinner_RedPotCount = (NumberPicker) dialog.findViewById(R.id.spinner_RedPotCount);
        spinner_RedPotCount.setMaxValue(6);
        spinner_RedPotCount.setMinValue(0);
        spinner_RedPotCount.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS); // stops the keypad appearing

        View.OnClickListener DialogOnClickListener = new View.OnClickListener()
        {
	        void Foul(int Score)
	        {
		        if (Player1Foul)
			        Player2Score += Score;
		        else
			        Player1Score += Score;

		        Shot NewShot;
		        NewShot = new Shot(GetCurrentPlayerID());
		        NewShot.SetFoul(true);

		        CurrentBreak.AddShot(NewShot);

		        Player1Turn = ! Player1Turn;

		        int SelectedVal = spinner_RedPotCount.getValue();

		        if(BallsOnTable.get(Balls.Red) - SelectedVal <= 0)
		        {
			        BallsOnTable.put(Balls.Red, 0);
			        if (CurrentBall == 1) CurrentBall++;
		        }
		        else
		        {
			        BallsOnTable.put(Balls.Red, BallsOnTable.get(Balls.Red) - SelectedVal);
		        }

		        AddEndLine();

		        FrameVector.get(FrameVector.size() - 1).AddBreak(new Break(CurrentBreak)); //adds break to end of latest frame
		        CurrentBreak = new Break(GetCurrentPlayerID());

		        //BreakBallHolder.removeAllViews();

		        RedPotted = false;
	        }

	        @Override
            public void onClick(View v)
            {
                switch(v.getId())
                {
                    case R.id.btn_StandardFoul:
                        SaveGameState();

						Foul(4);

	                    EndTurn();
                        dialog.hide();
                        break;
                    case R.id.btn_BlueFoul:

                        SaveGameState();

                        Foul(5);

	                    EndTurn();
                        dialog.hide();
                        break;
                    case R.id.btn_PinkFoul:

                        SaveGameState();

						Foul(6);

	                    EndTurn();
                        dialog.hide();
                        break;
                    case R.id.btn_BlackFoul:

                        SaveGameState();

	                    Foul(7);

	                    EndTurn();
                        dialog.hide();
                        break;
                }
            }
        };

        Button btn_StandardFoul = (Button) dialog.findViewById(R.id.btn_StandardFoul);
        btn_StandardFoul.setOnClickListener(DialogOnClickListener);

        Button btn_BlueFoul = (Button) dialog.findViewById(R.id.btn_BlueFoul);
        btn_BlueFoul.setOnClickListener(DialogOnClickListener);
        btn_BlueFoul.setBackgroundResource(R.drawable.blue);

        Button btn_PinkFoul = (Button) dialog.findViewById(R.id.btn_PinkFoul);
        btn_PinkFoul.setOnClickListener(DialogOnClickListener);
        btn_PinkFoul.setBackgroundResource(R.drawable.pink);

        Button btn_BlackFoul = (Button) dialog.findViewById(R.id.btn_BlackFoul);
        btn_BlackFoul.setOnClickListener(DialogOnClickListener);
        btn_BlackFoul.setBackgroundResource(R.drawable.black);

        dialog.show();
    }

    private void CreateMultiRedDialog()
    {
        AlertDialog.Builder AlertDialogBuilder = new AlertDialog.Builder(this);
        AlertDialogBuilder.setMessage("How many reds did you pot?");

        LayoutInflater Inflater = this.getLayoutInflater();
        final View DialogView = Inflater.inflate(R.layout.dialog_frame_multiball, null);
        AlertDialogBuilder.setView(DialogView);

        final NumberPicker numberPicker = (NumberPicker) DialogView.findViewById(R.id.numberPicker);
        numberPicker.setMaxValue(6);
        numberPicker.setMinValue(2);
        numberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS); // stops the keypad appearing


        AlertDialogBuilder.setPositiveButton("Confirm", new DialogInterface.OnClickListener()
        {
	        @Override
	        public void onClick(DialogInterface dialog, int which)
	        {
		        SaveGameState();

		        int BallCount = 0;
		        boolean GetsToZero = false;

		        if (BallsOnTable.get(Balls.Red) - numberPicker.getValue() > 0)
			        BallCount = numberPicker.getValue();
		        else
		        {
			        BallCount = BallsOnTable.get(Balls.Red);
			        GetsToZero = true;
		        }

		        for (int i = 0; i < BallCount; i++)
		        {
			        PotBall(Balls.Red, false);
		        }
		        RedPotted = true;

		        if (GetsToZero)
			        CurrentBall = 1;

		        EndTurn();
	        }
        });
        AlertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
	        @Override
	        public void onClick(DialogInterface dialog, int which)
	        {
		        // Dont do anything!
	        }
        });

        AlertDialog AlertDia = AlertDialogBuilder.create();
        AlertDia.show();
    }

    private void CreateFreeBallDialog()
    {
        AlertDialog.Builder AlertDialogBuilder = new AlertDialog.Builder(this);
        AlertDialogBuilder.setMessage("Free Ball");

        LayoutInflater Inflater = this.getLayoutInflater();
        final View DialogView = Inflater.inflate(R.layout.dialog_frame_freeball, null);
        AlertDialogBuilder.setView(DialogView);

        final AlertDialog AlertDia = AlertDialogBuilder.create();

        View.OnClickListener onClickListener = new View.OnClickListener()
        {
            Balls Ball;
            ImageView NewImg;
            RelativeLayout.LayoutParams params;

	        void PotFreeBall(Balls Ball)
	        {
		        if (Player1Turn)
			        Player1Score += CurrentBall;
		        else
			        Player2Score += CurrentBall;

		        Shot NewShot;
		        NewShot = new Shot(GetCurrentPlayerID());
		        NewShot.PotBall(Ball);
                NewShot.SetFreeBall(true, CurrentBall);

		        CurrentBreak.AddShot(NewShot);

		        AddBall(Ball);

		        RedPotted = true;
	        }

            @Override
            public void onClick(View v)
            {
                switch (v.getId())
                {
                    case R.id.btn_yellow:
                        SaveGameState();

	                    PotFreeBall(Balls.Yellow);

	                    EndTurn();
                        AlertDia.hide();
                        break;
                    case R.id.btn_green:
                        SaveGameState();

	                    PotFreeBall(Balls.Green);

	                    EndTurn();
                        AlertDia.hide();
                        break;
                    case R.id.btn_brown:
                        SaveGameState();

	                    PotFreeBall(Balls.Brown);

	                    EndTurn();
                        AlertDia.hide();
                        break;
                    case R.id.btn_blue:
                        SaveGameState();

	                    PotFreeBall(Balls.Blue);

	                    EndTurn();
                        AlertDia.hide();
                        break;
                    case R.id.btn_pink:
                        SaveGameState();

	                    PotFreeBall(Balls.Pink);

	                    EndTurn();
                        AlertDia.hide();
                        break;
                    case R.id.btn_black:
	                    SaveGameState();

	                    PotFreeBall(Balls.Black);

	                    EndTurn();
                        AlertDia.hide();
                        break;
                }
            }
        };

        Button btn_yellow = (Button) DialogView.findViewById(R.id.btn_yellow);
        btn_yellow.setOnClickListener(onClickListener);
        btn_yellow.setBackgroundResource(R.drawable.yellow);

        Button btn_green = (Button) DialogView.findViewById(R.id.btn_green);
        btn_green.setOnClickListener(onClickListener);
        btn_green.setBackgroundResource(R.drawable.green);

        Button btn_brown = (Button) DialogView.findViewById(R.id.btn_brown);
        btn_brown.setOnClickListener(onClickListener);
        btn_brown.setBackgroundResource(R.drawable.brown);

        Button btn_blue = (Button) DialogView.findViewById(R.id.btn_blue);
        btn_blue.setOnClickListener(onClickListener);
        btn_blue.setBackgroundResource(R.drawable.blue);

        Button btn_pink = (Button) DialogView.findViewById(R.id.btn_pink);
        btn_pink.setOnClickListener(onClickListener);
        btn_pink.setBackgroundResource(R.drawable.pink);

        Button btn_black = (Button) DialogView.findViewById(R.id.btn_black);
        btn_black.setOnClickListener(onClickListener);
        btn_black.setBackgroundResource(R.drawable.black);

        AlertDia.show();
    }

    private void SaveGameState()
    {
	    ArrayList<Frame> NewFrameVector = new ArrayList<>();
	    for(Frame p : FrameVector)
	    {
			NewFrameVector.add(new Frame(p));
	    }

        GameStatus CurrentStatus = new GameStatus
        (
	        new HashMap<>(BallsOnTable),
	        new Break(CurrentBreak),
	        CurrentBall,
	        NewFrameVector,
	        Player1Turn,
	        RedPotted,
	        Player1Score,
	        Player2Score,
	        Player1Frames,
	        Player2Frames,
	        new Player(PSave.Players.get(Player1ID)),
	        new Player(PSave.Players.get(Player2ID))
        );

	    GameStatusHolder.add(CurrentStatus);
    }

    private void RevertGameState()
    {
        if (GameStatusHolder.size() > 0)
        {
            GameStatus PreviousStatus = GameStatusHolder.get(GameStatusHolder.size() - 1);
            GameStatusHolder.remove(GameStatusHolder.size() - 1);

	        // Changes players stats to old stats if there was a frame gap in between
	        if (!((Player1Frames == PreviousStatus.Player1Frames)&(Player2Frames == PreviousStatus.Player2Frames))) // if a frame was ended
	        {
		        PSave.Players.put(Player1ID, PreviousStatus.Player1);
		        PSave.Players.put(Player2ID, PreviousStatus.Player2);

		        PSave.ReplacePlayers(PlayGame.this);
	        }
	        //

            BallsOnTable = PreviousStatus.BallsOnTable;

            CurrentBreak = PreviousStatus.CurrentBreak;
	        FrameVector = new ArrayList<>(PreviousStatus.FrameVector);
            CurrentBall = PreviousStatus.CurrentBall;

            Player1Turn = PreviousStatus.Player1Turn;
            RedPotted = PreviousStatus.RedPotted;

            Player1Score = PreviousStatus.Player1Score;
            Player2Score = PreviousStatus.Player2Score;
            Player1Frames = PreviousStatus.Player1Frames;
            Player2Frames = PreviousStatus.Player2Frames;

	        BreakBallHolder.removeAllViews();

	        int CurFrame = Player1Frames + Player2Frames;

            for (int i = 0; i < FrameVector.get(CurFrame).GetBreakVector().size(); i++)//Get the breaks from the frame
            {
                for (int j = 0; j < FrameVector.get(CurFrame).GetBreakVector().get(i).GetShots().size(); j++)//get the shots from the break
                {
	                if (FrameVector.get(CurFrame).GetBreakVector().get(i).GetShots().get(j).GetPotted() == null)
	                {
		                AddEndLine();
	                }
	                else
	                {
		                AddBall(FrameVector.get(CurFrame).GetBreakVector().get(i).GetShots().get(j).GetPotted());
	                }
                }
            }

            for (int i = 0; i < CurrentBreak.GetShots().size(); i++)
            {
                AddBall(CurrentBreak.GetShots().get(i).GetPotted());
            }
        }
    }

    private void UpdateDisplays()
    {
        txt_Player1Score.setText(String.valueOf(Player1Score));
        txt_Player2Score.setText(String.valueOf(Player2Score));
        txt_BreakCounter.setText(String.valueOf(CurrentBreak.GetValue()));

        if (Player1Turn)
        {
            txt_Player1Name.setTextColor(-256);
            txt_Player2Name.setTextColor(-1);

	        Player1Triangle.setVisibility(View.VISIBLE);
	        Player2Triangle.setVisibility(View.INVISIBLE);
        }
        else
        {
            txt_Player1Name.setTextColor(-1);
            txt_Player2Name.setTextColor(-256);

	        Player1Triangle.setVisibility(View.INVISIBLE);
	        Player2Triangle.setVisibility(View.VISIBLE);
        }

        String Value = String.valueOf(Math.abs(Player1Score - Player2Score));
        if (Player1Score > Player2Score) // Player 1 winning
        {
            txt_Player1Ahead.setText("Ahead: " + Value);
            txt_Player2Ahead.setText("Behind: " + Value);
        }
        else if (Player2Score > Player1Score) // Player 2 winning
        {
            txt_Player2Ahead.setText("Ahead: " + Value);
            txt_Player1Ahead.setText("Behind: " + Value);
        }
        else // draw
        {
            txt_Player1Ahead.setText("Drawing");
            txt_Player2Ahead.setText("Drawing");
        }
        //}

        txt_Player1FrameCount.setText(String.valueOf(Player1Frames));
        txt_Player2FrameCount.setText(String.valueOf(Player2Frames));

        txt_PointsRemaining.setText(String.valueOf(CalcPointsRemaining()) + " Points Remaining");
    }

    private void UpdateButtons()
    {
        btn_FinishFrame.setVisibility(View.INVISIBLE);
        btn_Foul.setVisibility(View.VISIBLE);
        btn_WhiteFlag.setVisibility(View.VISIBLE);
        btn_EndBreak.setVisibility(View.VISIBLE);

        btn_RedBall.setText(String.valueOf(BallsOnTable.get(Balls.Red)));
        if (RedPotted & CurrentBall == 1) // If the next pot is a color or red
        {
            btn_RedBall.setVisibility(View.INVISIBLE);

            btn_YellowBall.setVisibility(View.VISIBLE);
            btn_GreenBall.setVisibility(View.VISIBLE);
            btn_BrownBall.setVisibility(View.VISIBLE);
            btn_BlueBall.setVisibility(View.VISIBLE);
            btn_PinkBall.setVisibility(View.VISIBLE);
            btn_BlackBall.setVisibility(View.VISIBLE);
        }
        else if(BallsOnTable.get(Balls.Red) != 0)
        {
            btn_RedBall.setVisibility(View.VISIBLE);

            btn_YellowBall.setVisibility(View.INVISIBLE);
            btn_GreenBall.setVisibility(View.INVISIBLE);
            btn_BrownBall.setVisibility(View.INVISIBLE);
            btn_BlueBall.setVisibility(View.INVISIBLE);
            btn_PinkBall.setVisibility(View.INVISIBLE);
            btn_BlackBall.setVisibility(View.INVISIBLE);
        }
        else
        {
            if (CurrentBall == 2)
            {
                btn_YellowBall.setVisibility(View.VISIBLE);

                btn_RedBall.setVisibility(View.INVISIBLE);
                btn_GreenBall.setVisibility(View.INVISIBLE);
                btn_BrownBall.setVisibility(View.INVISIBLE);
                btn_BlueBall.setVisibility(View.INVISIBLE);
                btn_PinkBall.setVisibility(View.INVISIBLE);
                btn_BlackBall.setVisibility(View.INVISIBLE);
            }
            else if (CurrentBall == 3)
            {
                btn_GreenBall.setVisibility(View.VISIBLE);

                btn_RedBall.setVisibility(View.INVISIBLE);
                btn_YellowBall.setVisibility(View.INVISIBLE);
                btn_BrownBall.setVisibility(View.INVISIBLE);
                btn_BlueBall.setVisibility(View.INVISIBLE);
                btn_PinkBall.setVisibility(View.INVISIBLE);
                btn_BlackBall.setVisibility(View.INVISIBLE);

            }
            else if (CurrentBall == 4)
            {
                btn_BrownBall.setVisibility(View.VISIBLE);

                btn_RedBall.setVisibility(View.INVISIBLE);
                btn_YellowBall.setVisibility(View.INVISIBLE);
                btn_GreenBall.setVisibility(View.INVISIBLE);
                btn_BlueBall.setVisibility(View.INVISIBLE);
                btn_PinkBall.setVisibility(View.INVISIBLE);
                btn_BlackBall.setVisibility(View.INVISIBLE);
            }
            else if (CurrentBall == 5)
            {
                btn_BlueBall.setVisibility(View.VISIBLE);

                btn_RedBall.setVisibility(View.INVISIBLE);
                btn_YellowBall.setVisibility(View.INVISIBLE);
                btn_GreenBall.setVisibility(View.INVISIBLE);
                btn_BrownBall.setVisibility(View.INVISIBLE);
                btn_PinkBall.setVisibility(View.INVISIBLE);
                btn_BlackBall.setVisibility(View.INVISIBLE);
            }
            else if (CurrentBall == 6)
            {
                btn_PinkBall.setVisibility(View.VISIBLE);

                btn_RedBall.setVisibility(View.INVISIBLE);
                btn_YellowBall.setVisibility(View.INVISIBLE);
                btn_GreenBall.setVisibility(View.INVISIBLE);
                btn_BrownBall.setVisibility(View.INVISIBLE);
                btn_BlueBall.setVisibility(View.INVISIBLE);
                btn_BlackBall.setVisibility(View.INVISIBLE);
            }
            else if (CurrentBall == 7)
            {
                btn_BlackBall.setVisibility(View.VISIBLE);

                btn_RedBall.setVisibility(View.INVISIBLE);
                btn_YellowBall.setVisibility(View.INVISIBLE);
                btn_GreenBall.setVisibility(View.INVISIBLE);
                btn_BrownBall.setVisibility(View.INVISIBLE);
                btn_BlueBall.setVisibility(View.INVISIBLE);
                btn_PinkBall.setVisibility(View.INVISIBLE);
            }
            else if (CurrentBall >= 8)
            {
                btn_FinishFrame.setVisibility(View.VISIBLE);

                btn_RedBall.setVisibility(View.INVISIBLE);
                btn_YellowBall.setVisibility(View.INVISIBLE);
                btn_GreenBall.setVisibility(View.INVISIBLE);
                btn_BrownBall.setVisibility(View.INVISIBLE);
                btn_BlueBall.setVisibility(View.INVISIBLE);
                btn_PinkBall.setVisibility(View.INVISIBLE);
                btn_BlackBall.setVisibility(View.INVISIBLE);

                btn_Foul.setVisibility(View.INVISIBLE);
                btn_WhiteFlag.setVisibility(View.INVISIBLE);
                btn_EndBreak.setVisibility(View.INVISIBLE);
            }

        }
    }

    private int CalcPointsRemaining()
    {
        if (CurrentBall == 1)
        {
            if (!RedPotted) // Red is potted so colors are showing
                return 27 + (BallsOnTable.get(Balls.Red) * 8);
            else
                return 27 + (BallsOnTable.get(Balls.Red) * 8) + 7;
        }
        else
        {
            return (BallsOnTable.get(Balls.Black) * 7) + (BallsOnTable.get(Balls.Pink) * 6) + (BallsOnTable.get(Balls.Blue) * 5)
                 + (BallsOnTable.get(Balls.Brown) * 4) + (BallsOnTable.get(Balls.Green) * 3) + (BallsOnTable.get(Balls.Yellow) * 2);
        }

    }

    private void AddEndLine()
    {
        ImageView NewImg = new ImageView(this);
        NewImg.setLayoutParams(new RelativeLayout.LayoutParams(3, 34));
        NewImg.setImageResource(R.drawable.end);
        BreakBallHolder.addView(NewImg);
    }

    private void AddBall(Balls Ball)
    {
        ImageView NewImg = new ImageView(this);

        NewImg.setLayoutParams(new RelativeLayout.LayoutParams(32, ViewGroup.LayoutParams.MATCH_PARENT)); // size of the image

        if (Ball == Balls.Red)
            NewImg.setImageResource(R.drawable.red);
        else if (Ball == Balls.Yellow)
            NewImg.setImageResource(R.drawable.yellow);
        else if (Ball == Balls.Green)
            NewImg.setImageResource(R.drawable.green);
        else if (Ball == Balls.Brown)
            NewImg.setImageResource(R.drawable.brown);
        else if (Ball == Balls.Blue)
            NewImg.setImageResource(R.drawable.blue);
        else if (Ball == Balls.Pink)
            NewImg.setImageResource(R.drawable.pink);
        else
            NewImg.setImageResource(R.drawable.black);

        BreakBallHolder.addView(NewImg); // Adds new ball to layout
    }

	private String GetCurrentPlayerID()
	{
		if (!Player1Turn)
			return Player1ID;
		else
			return Player2ID;
	}

    private void SaveStateVector()
    { // should directly overwrite the old one
        String FilePath = this.getFilesDir().getPath() + "/saves/" + String.valueOf(FrameID);
	    try
	    {
		    File file = new File(FilePath);
		    if (!file.exists())
		    {
				file.createNewFile();

			    ObjectOutputStream ObjStream = new ObjectOutputStream(new FileOutputStream(FilePath));
				ObjStream.writeObject(GameStatusHolder);
			    ObjStream.flush();
			    ObjStream.close();
		    }
	    }
	    catch(IOException e)
	    {
		    e.printStackTrace();
	    }
	    catch (Exception e)
	    {
		    e.printStackTrace();
	    }
    }

	private void EndTurn()
	{
		SaveStateVector();

		UpdateDisplays();
		UpdateButtons();
	}
}
