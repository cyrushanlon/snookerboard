package com.example.cyrus.snookerboard;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Created by Cyrus Laptop on 27/05/2015.
 */

public class AppendObjectOutputStream extends ObjectOutputStream
{

    public AppendObjectOutputStream(FileOutputStream out) throws Exception
    {
        super(out);
    }

    @Override
    protected void writeStreamHeader() throws IOException
    {
        // do not write a header, but reset:
        // this line added after another question
        // showed a problem with the original
        reset();
    }

}