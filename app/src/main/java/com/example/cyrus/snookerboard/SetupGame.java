package com.example.cyrus.snookerboard;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;


public class SetupGame extends AppCompatActivity
{
	private Button btn_PlayGame;
	private NumberPicker numberPicker;

	private ListView lv_PlayerListView1, lv_PlayerListViewTitle1, lv_PlayerListView2, lv_PlayerListViewTitle2;
	private ArrayList<HashMap<String, String>> PlayerlistArray, PlayerListTitleArray;
	private HashMap<String, String> PlayerListMap, PlayerListTitleMap;

	Player Player1,Player2;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setup_game);

		numberPicker = (NumberPicker)findViewById(R.id.numberPicker);
		numberPicker.setMaxValue(100);
		numberPicker.setMinValue(1);
		numberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS); // stops the keypad appearing

		btn_PlayGame = (Button)findViewById(R.id.btn_PlayGame);
		btn_PlayGame.setOnClickListener(onClickListener);

		lv_PlayerListView1 = (ListView)findViewById(R.id.lv_PlayerListView1);
		lv_PlayerListView1.setOnItemClickListener(Player1ItemClick);

		lv_PlayerListViewTitle1 = (ListView)findViewById(R.id.lv_PlayerListViewTitle1);

		lv_PlayerListView2 = (ListView)findViewById(R.id.lv_PlayerListView2);
		lv_PlayerListView2.setOnItemClickListener(Player2ItemClick);

		lv_PlayerListViewTitle2 = (ListView)findViewById(R.id.lv_PlayerListViewTitle2);

		PSave.LoadPlayers(this);
		PopulateListViews();
	}

	private ListView.OnItemClickListener Player1ItemClick = new ListView.OnItemClickListener()
	{
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int Pos, long arg3)
		{
			TextView ID = (TextView)view.findViewById(R.id.ID);
			Player1 = PSave.Players.get(ID.getText().toString());
		}
	};

	private ListView.OnItemClickListener Player2ItemClick = new ListView.OnItemClickListener()
	{
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int Pos, long arg3)
		{
			TextView ID = (TextView)view.findViewById(R.id.ID);
			Player2 = PSave.Players.get(ID.getText().toString());
		}
	};

	private View.OnClickListener onClickListener = new View.OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			switch(v.getId()){
				case R.id.btn_PlayGame:
					Intent i = new Intent(SetupGame.this, PlayGame.class);
					i.putExtra("FrameCount", (numberPicker.getValue() * 2) - 1);
					i.putExtra("Player1ID", Player1.GetID());
					i.putExtra("Player2ID", Player2.GetID());
					startActivity(i);
					break;
			}
		}
	};

	private void PopulateListViews()
	{
		ListAdapter PlayerListAdapter, PlayerListTitleAdapter;

		lv_PlayerListViewTitle1.setAdapter(null);
		lv_PlayerListViewTitle2.setAdapter(null);
		lv_PlayerListView1.setAdapter(null);
		lv_PlayerListView2.setAdapter(null);

		PlayerlistArray = new ArrayList<>();
		PlayerListTitleArray = new ArrayList<>();

		//Sets up the title of the columns
		PlayerListTitleMap = new HashMap<>();
		PlayerListTitleMap.put("ID", "ID");
		PlayerListTitleMap.put("first", "First Name");
		PlayerListTitleMap.put("second", "Second Name");
		PlayerListTitleArray.add(PlayerListTitleMap);

		//loop through vector
		for(Player Ply : PSave.Players.values())
		{
			PlayerListMap = new HashMap<>();
			PlayerListMap.put("ID", String.valueOf(Ply.GetID()));
			PlayerListMap.put("first", Ply.GetFirstName());
			PlayerListMap.put("second", Ply.GetSecondName());
			PlayerlistArray.add(PlayerListMap);
		}

		try // Adds titles to listview
		{
			PlayerListTitleAdapter = new SimpleAdapter(this, PlayerListTitleArray, R.layout.player_info_row, new String[]{"ID","first","second"}, new int[]{R.id.ID, R.id.first, R.id.second});
			lv_PlayerListViewTitle1.setAdapter(PlayerListTitleAdapter);
			lv_PlayerListViewTitle2.setAdapter(PlayerListTitleAdapter);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return;
		}

		try // Adds players to listview
		{
			PlayerListAdapter = new SimpleAdapter(this, PlayerlistArray, R.layout.player_info_row, new String[]{"ID","first","second"}, new int[]{R.id.ID, R.id.first, R.id.second});
			lv_PlayerListView1.setAdapter(PlayerListAdapter);
			lv_PlayerListView2.setAdapter(PlayerListAdapter);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return;
		}
	}
}
