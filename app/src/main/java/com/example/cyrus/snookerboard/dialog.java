package com.example.cyrus.snookerboard;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TabHost;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

/**
 * Created by Cyrus on 04/06/2015.
 */
public class dialog
{

	public static void CreateStatDialog(Context This, Player Ply)
	{
		AlertDialog.Builder AlertDialogBuilder = new AlertDialog.Builder(This);
		AlertDialogBuilder.setMessage(Ply.GetFirstName() + " " + Ply.GetSecondName() + " Statistics");

		LayoutInflater Inflater = (LayoutInflater) This.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View DialogView = Inflater.inflate(R.layout.dialog_player_panel, null);
		AlertDialogBuilder.setView(DialogView);

		final AlertDialog self = AlertDialogBuilder.create();

		ListView lv_Stat = (ListView) DialogView.findViewById(R.id.lv_Stat);
		ListView lv_StatTitle = (ListView) DialogView.findViewById(R.id.lv_StatTitle);

		ListView lv_Break = (ListView) DialogView.findViewById(R.id.lv_Break);
		ListView lv_BreakTitle = (ListView) DialogView.findViewById(R.id.lv_BreakTitle);
		//lv_Stat.setOnClickListener(onClickListener);

		TabHost host = (TabHost)DialogView.findViewById(R.id.TabHost);
		host.setup();

		//tab 1 ////////////////////////////////////////////////////////////////////////////////////

		TabHost.TabSpec spec = host.newTabSpec("Tab One");
		spec.setContent(R.id.tab1);
		spec.setIndicator("Stats");
		host.addTab(spec);

		//Populate list view

		ListAdapter PlayerListAdapter, PlayerListTitleAdapter;

		lv_Stat.setAdapter(null);
		lv_StatTitle.setAdapter(null);

		ArrayList<HashMap<String, String>> PlayerlistArray = new ArrayList<>();
		ArrayList<HashMap<String, String>> PlayerListTitleArray = new ArrayList<>();

		//Sets up the title of the columns
		HashMap<String, String> PlayerListMap = new HashMap<>();
		PlayerListMap.put("StatName", "Stat Name");
		PlayerListMap.put("StatVal", "Stat Value");
		PlayerListTitleArray.add(PlayerListMap);

		try // Adds titles to listview
		{
			PlayerListTitleAdapter = new SimpleAdapter(This, PlayerListTitleArray, R.layout.player_stat_row, new String[]{"StatName","StatVal"}, new int[]{R.id.StatName, R.id.StatVal});
			lv_StatTitle.setAdapter(PlayerListTitleAdapter);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return;
		}

		// Treemap<String<Treemap<String,Integer>>
		for (TreeMap.Entry<String, TreeMap<String, Integer>> Entry : Ply.GetAllStats().entrySet()) // goes through and adds all stats
		{
			for (TreeMap.Entry<String, Integer> StatEntry : Entry.getValue().entrySet())
			{
				Integer Val = StatEntry.getValue();
				String Key = StatEntry.getKey();

				PlayerListMap = new HashMap<>();
				PlayerListMap.put("StatName", Key);
				PlayerListMap.put("StatVal", String.valueOf(Val));
				PlayerlistArray.add(PlayerListMap);
			}

			if (Entry.getKey().equals("General")) // adds shot accuracy
			{
				//if ((Entry.getValue().get("Shots Taken") != null) & (Entry.getValue().get("Shots Missed") != null))
				//{
					if ((Entry.getValue().get("Shots Taken") > 0) && (Entry.getValue().get("Shots Missed") != null))// prevents div 0 and nullpointer
					{
						PlayerListMap = new HashMap<>();
						PlayerListMap.put("StatName", "Shot Accuracy");
						PlayerListMap.put("StatVal", String.valueOf((double) Entry.getValue().get("Shots Missed") / (double) Entry.getValue().get("Shots Taken") * 100) + "%");
						PlayerlistArray.add(PlayerListMap);
					}
				//}
			}

			PlayerListMap = new HashMap<>();
			PlayerListMap.put("StatName", "");
			PlayerListMap.put("StatVal", "");
			PlayerlistArray.add(PlayerListMap);
		}

		//

		try // Adds rows to listview
		{
			PlayerListAdapter = new SimpleAdapter(This, PlayerlistArray, R.layout.player_stat_row, new String[]{"StatName","StatVal"}, new int[]{R.id.StatName, R.id.StatVal});
			lv_Stat.setAdapter(PlayerListAdapter);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return;
		}

		//tab 2 ////////////////////////////////////////////////////////////////////////////////////

		spec = host.newTabSpec("Tab Two");
		spec.setContent(R.id.tab2);
		spec.setIndicator("Breaks");
		host.addTab(spec);

		//Populate list view

		lv_Break.setAdapter(null);
		lv_BreakTitle.setAdapter(null);

		PlayerlistArray = new ArrayList<>();
		PlayerListTitleArray = new ArrayList<>();

		//Sets up the title of the columns
		PlayerListMap = new HashMap<>();
		PlayerListMap.put("Position", "Position");
		PlayerListMap.put("Size", "Size of break");
		PlayerListMap.put("Date", "Date Completed");
		PlayerListTitleArray.add(PlayerListMap);

		try // Adds titles to listview
		{
			PlayerListTitleAdapter = new SimpleAdapter(This, PlayerListTitleArray, R.layout.player_info_row, new String[]{"Position","Size", "Date"}, new int[]{R.id.ID, R.id.first, R.id.second});
			lv_BreakTitle.setAdapter(PlayerListTitleAdapter);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return;
		}

		//
		for (int i = 0; i < Ply.GetBreakStats().size(); i++) // goes through and adds all stats
		{
			PlayerListMap = new HashMap<>();
			PlayerListMap.put("Position", String.valueOf(i));
			PlayerListMap.put("Size", String.valueOf(Ply.GetBreakStats().get(i).GetValue()));
			PlayerListMap.put("Date", String.valueOf(Ply.GetBreakStats().get(i).GetDateInSeconds())); // needs changing to a date no time
			PlayerlistArray.add(PlayerListMap);
		}
		//

		try // Adds rows to listview
		{
			PlayerListAdapter = new SimpleAdapter(This, PlayerlistArray, R.layout.player_info_row, new String[]{"Position","Size", "Date"}, new int[]{R.id.ID, R.id.first, R.id.second});
			lv_Break.setAdapter(PlayerListAdapter);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return;
		}

		self.show();
	}
}
