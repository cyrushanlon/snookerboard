package com.example.cyrus.snookerboard;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

/**
 * Created by Cyrus on 10/06/2015.
 */
public class Break implements Serializable
{
	public Break(Break NewBreak)
	{
		this.Shots = new Vector<>(NewBreak.GetShots());
		this.DateCompleted = NewBreak.GetDateInSeconds();
		this.Value = NewBreak.GetValue();
		this.PlayerID = NewBreak.GetPlayerID();
	}
	public Break(String PlayerID)
	{//set everything to default
		this.Shots = new Vector<>();
		this.DateCompleted = 0;
		this.Value = 0;
		this.PlayerID = PlayerID;
	}

	private int DateCompleted;
	private Vector<Shot> Shots;
	private int Value;
	private String PlayerID;

	public int GetDateInSeconds() { return this.DateCompleted;}
	public Vector<Shot> GetShots() { return this.Shots;}
	public int GetValue() { return this.Value;}
	public String GetPlayerID() { return this.PlayerID;}

	public void AddShot(Shot NewShot)
	{
		this.Shots.add(NewShot);
		if (NewShot.GetFreeBall())
			Value += NewShot.GetFreeBallValue();
		else if (NewShot.GetPotted() != null & !NewShot.GetFoul()) // if shot is valid add points to break value
			Value += NewShot.GetPotted().GetVal();
	}

	/*public Break clone()
	{
		try
		{
			Break BreakClone = (Break) super.clone();
			return BreakClone;
		}
		catch(CloneNotSupportedException e)
		{
			e.printStackTrace();
			return null;
		}
	}*/

	public Break DeepClone()
	{
		try
		{
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(this);

			ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bais);
			return (Break) ois.readObject();
		}
		catch (IOException e)
		{
			return null;
		}
		catch (ClassNotFoundException e)
		{
			return null;
		}
	}
}
