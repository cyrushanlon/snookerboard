package com.example.cyrus.snookerboard;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Vector;

/**
 * Created by Cyrus on 01/06/2015.
 */
public class PSave
{
	public static HashMap<String, Player> Players;
	private static final String FileName = "/playerfile.ser";

	public static void LoadPlayers(Context This)
	{
		Players = new HashMap<>();
		File file = new File( This.getFilesDir().getPath() + FileName);
		if (!file.exists()) //returns if the file doesnt exist
		{
			return;
		}

		ObjectInputStream InStream;

		//loads from file and displays the players
		try
		{
			Player Ply = null;
			Object Obj = null;

			InStream = new ObjectInputStream(new FileInputStream( This.getFilesDir().getPath() + FileName));

			while ((Obj = InStream.readObject()) != null)
			{
				Ply = (Player) Obj;
				Players.put(String.valueOf(Ply.GetID()), Ply);
			}
			InStream.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
			Players.clear();
			return;
		}
	}

	public static void DeletePlayerFile(Context This)
	{
		File PlayerFile = new File( This.getFilesDir().getPath() + FileName);
		PlayerFile.delete();
	}

	public static void SavePlayer(Context This, Player NewPly)
	{
		try
		{
			File file = new File( This.getFilesDir().getPath() + FileName);
			if (!file.exists())
			{
				file.createNewFile();

				ObjectOutputStream ObjStream = new ObjectOutputStream(new FileOutputStream( This.getFilesDir().getPath() + FileName, true));
				ObjStream.writeObject(NewPly);
				ObjStream.flush();
				ObjStream.close();
			}
			else
			{
				AppendObjectOutputStream ObjStream = new AppendObjectOutputStream(new FileOutputStream( This.getFilesDir().getPath() + FileName, true));
				ObjStream.writeObject(NewPly);
				ObjStream.flush();
				ObjStream.close();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void ReplacePlayers(Context This)
	{
		DeletePlayerFile(This);

		try
		{
			File file = new File(This.getFilesDir().getPath() + FileName);
			if (!file.exists())
			{
				file.createNewFile();

				ObjectOutputStream ObjStream = new ObjectOutputStream(new FileOutputStream(This.getFilesDir().getPath() + FileName, true));
				for(Player NewPly : Players.values())
				{
					ObjStream.writeObject(NewPly);
				}
				ObjStream.flush();
				ObjStream.close();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
