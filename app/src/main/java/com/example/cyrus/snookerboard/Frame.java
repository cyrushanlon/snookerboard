package com.example.cyrus.snookerboard;

import java.util.Vector;

/**
 * Created by Cyrus on 22/06/2015.
 */
public class Frame
{
	public Frame()
	{
		BreakVector = new Vector<>();
	}
	public Frame(Frame NewFrame)
	{
		BreakVector = new Vector<>();

		for(Break p : NewFrame.GetBreakVector())
		{
			BreakVector.add(p.DeepClone());
		}
	}

	private Vector<Break> BreakVector;

	public Vector<Break> GetBreakVector()
	{
		return BreakVector;
	}
	public void AddBreak(Break NewBreak)
	{
		BreakVector.add(NewBreak);
	}
}
