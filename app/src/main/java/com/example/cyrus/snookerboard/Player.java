package com.example.cyrus.snookerboard;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.Vector;

/**
 * Created by Cyrus on 19/05/2015.
 */
public class Player implements Serializable
{
    public Player(String NewID, String First, String Second)
    {
        this.ID = NewID;
	    this.FirstName = First;
	    this.SecondName = Second;

	    Stats = new TreeMap<>();

	    BreakHolder = new ArrayList<>();
    }
	public Player(Player NewPly)
	{
		ID = NewPly.GetID();
		FirstName = NewPly.GetFirstName();
		SecondName = NewPly.GetSecondName();

		Stats = new TreeMap<>();

		BreakHolder = NewPly.GetBreakStats();
	}

    private String ID;
    private String FirstName,SecondName;

	private TreeMap<String,  TreeMap<String, Integer>> Stats;
	private ArrayList<Break> BreakHolder;

	public void ProcessFrame(ArrayList<Break> FrameVector, String Winner) // Gets stats from all shots in a game
	{
		for(int x = 0; x < FrameVector.size(); x++)// for each break
		{
			if (!FrameVector.get(x).GetPlayerID().equals(ID))
			{
				for (int y = 0; y < FrameVector.get(x).GetShots().size(); y++)// for each shot in break
				{
					Shot CurShot = FrameVector.get(x).GetShots().get(y);
					if (CurShot.GetPotted() == null || CurShot.GetFoul())// if something got potted and not a foul
					{
						if (CurShot.GetFoul()) //if the ball was a foul
						{
							SetStat("Shots Fouled", 1, "General");
						}
						SetStat("Shots Missed", 1, "General");
					} else //valid pot
					{
						SetStat(CurShot.GetPotted().name() + " Pots", 1, "Pots");
					}

					SetStat("Shots Taken", 1, "General");
				}
				if (FrameVector.get(x).GetValue() > 10) // only add break to list if its greater than 10
				{
					SetStat("Breaks over 010", 1, "Breaks");
					BreakHolder.add(FrameVector.get(x)); // adds break to list
				}
				if (FrameVector.get(x).GetValue() > 20)
					SetStat("Breaks over 020", 1, "Breaks");
				if (FrameVector.get(x).GetValue() > 40)
					SetStat("Breaks over 040", 1, "Breaks");
				if (FrameVector.get(x).GetValue() > 60)
					SetStat("Breaks over 060", 1, "Breaks");
				if (FrameVector.get(x).GetValue() > 80)
					SetStat("Breaks over 080", 1, "Breaks");
				if (FrameVector.get(x).GetValue() > 100)
					SetStat("Breaks over 100", 1, "Breaks");
				if (FrameVector.get(x).GetValue() > 120)
					SetStat("Breaks over 120", 1, "Breaks");
				if (FrameVector.get(x).GetValue() > 140)
					SetStat("Breaks over 140", 1, "Breaks");
				if (FrameVector.get(x).GetValue() == 147)
					SetStat("Breaks of 147", 1, "Breaks");

				ResetStat("Total Pots", "Pots");
				SetStat("Total Pots", GetStat("Red Pots", "Pots") + GetStat("Yellow Pots", "Pots") + GetStat("Green Pots", "Pots") + GetStat("Brown Pots", "Pots") + GetStat("Blue Pots", "Pots") + GetStat("Pink Pots", "Pots") + GetStat("Black Pots", "Pots"), "Pots");
			}
		}

		if (ID.equals(Winner))
		{
			SetStat("Frame Wins", 1, "General");
		}
	}

    public String GetFirstName()
    {
	    return this.FirstName;
    }
    public String GetSecondName()
    {
	    return this.SecondName;
    }
	public String GetID()
	{
		return this.ID;
	}
	public int GetStat(String Key, String MapID)
	{
		if (Stats.get(MapID) != null)
			if (Stats.get(MapID).get(Key) != null)
				return Stats.get(MapID).get(Key);
			else
				return 0;
		else
			return 0;
	}
	public TreeMap<String, TreeMap<String, Integer>> GetAllStats() {return new TreeMap<>(this.Stats);}
	public ArrayList<Break> GetBreakStats()
	{
		return this.BreakHolder;
	}

	private void ResetStat(String Key, String MapID)
	{
		if (Stats.get(MapID) != null) // if stats already exists
			Stats.get(MapID).put(Key, null);
	}

	private void SetStat(String Key, int Val, String MapID)
	{
		if (Stats.get(MapID) == null)
			Stats.put(MapID, new TreeMap<String, Integer>());

		if (Stats.get(MapID).get(Key) != null)
			Stats.get(MapID).put(Key, GetStat(Key, MapID) + Val);
		else
			Stats.get(MapID).put(Key, Val);
	}

}
