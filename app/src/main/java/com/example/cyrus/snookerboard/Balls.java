package com.example.cyrus.snookerboard;

/**
 * Created by Cyrus on 21/05/2015.
 */
public enum Balls
{
    Red(1),
    Yellow(2),
    Green(3),
    Brown(4),
    Blue(5),
    Pink(6),
    Black(7);

    private Balls(int Val)
    {
        this.Value = Val;
    }
    public int GetVal()
    {
        return this.Value;
    }


    private final int Value;
}
