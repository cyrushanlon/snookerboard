package com.example.cyrus.snookerboard;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Cyrus on 21/05/2015.
 * Stores the status of the game at any given point allowing for undoing
 */
public class GameStatus implements Serializable
{
    public HashMap<Balls, Integer> BallsOnTable;
    public Break CurrentBreak;
    public ArrayList<Frame> FrameVector;
    public int CurrentBall;
    //public int CurrentBreakVal;

    public boolean Player1Turn; // Player 1 goes first
    public boolean RedPotted;

    public int Player1Score, Player2Score;
    public int Player1Frames, Player2Frames;

    public Player Player1,Player2;

    public GameStatus(HashMap<Balls, Integer> A, Break B, int C, ArrayList<Frame> E, boolean F, boolean G, int H, int I, int J, int K, Player L, Player M)
    {
        BallsOnTable = A;
        CurrentBreak = B;
        CurrentBall = C;
        //CurrentBreakVal = D;
        FrameVector = E;
        Player1Turn = F;
        RedPotted = G;
        Player1Score = H;
        Player2Score = I;
        Player1Frames = J;
        Player2Frames = K;
        Player1 = L; // might want to be changed later on
        Player2 = M; // might want to be changed later on
    }
}
