package com.example.cyrus.snookerboard;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;


public class Main extends AppCompatActivity
{
    Button btn_PlayFrame;
	Button btn_CreatePlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_PlayFrame = (Button)findViewById(R.id.btn_PlayFrame);
        btn_PlayFrame.setOnClickListener(onClickListener);

	    btn_CreatePlayer = (Button)findViewById(R.id.btn_CreatePlayer);
	    btn_CreatePlayer.setOnClickListener(onClickListener);
    }

    private OnClickListener onClickListener = new OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            switch(v.getId()){
                case R.id.btn_PlayFrame:
                    Intent i = new Intent(Main.this, SetupGame.class);
                    startActivity(i);
                    break;
	            case R.id.btn_CreatePlayer:
		            i = new Intent(Main.this, PlayerInfo.class);
		            startActivity(i);
		            break;
            }
        }
    };
}
