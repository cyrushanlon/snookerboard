package com.example.cyrus.snookerboard;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Cyrus on 02/06/2015.
 */
//Holds all information regarding all shots in PlayGame
public class Shot implements Serializable
{
	public Shot(String PlyID)
	{
		this.PlayerID = PlyID;
		//Defaults
		Ball = null;
		Foul = false;
		FreeBall = false;
	}

	//Vars
	private String PlayerID;
	private Balls Ball;

	private boolean Foul;
	private boolean FreeBall;
	private int FreeBallValue;

	//free ball stuff
	//Defs

	public void PotBall(Balls Ball)
	{
		this.Ball = Ball;
	}
	public void SetFoul(boolean B) {this.Foul = B;}
	public void SetFreeBall(boolean B, int V)
	{
		this.FreeBall = B;
		this.FreeBallValue = V;
	}

	public Balls GetPotted()
	{
		return this.Ball; // returns null when no pot occured
	}
	public String GetPlayerID() {return this.PlayerID;}
	public boolean GetFoul() {return this.Foul;}
	public boolean GetFreeBall() { return this.FreeBall;}
	public int GetFreeBallValue() { return this.FreeBallValue;}
}
