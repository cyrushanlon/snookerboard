package com.example.cyrus.snookerboard;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TabHost;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.Vector;


public class PlayerInfo extends AppCompatActivity
{
	private EditText etxt_FirstName, etxt_SecondName;

	private ListView lv_PlayerListView, lv_PlayerListViewTitle;
	private ListView lv_Stat, lv_StatTitle, lv_Break, lv_BreakTitle;
	private TabHost host;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_player_info);

		Button btn_Submit = (Button)findViewById(R.id.btn_Submit);
		btn_Submit.setOnClickListener(onClickListener);

		Button btn_Reset = (Button)findViewById(R.id.btn_Reset);
		btn_Reset.setOnClickListener(onClickListener);

		lv_PlayerListView = (ListView)findViewById(R.id.lv_PlayerListView);
		lv_PlayerListViewTitle = (ListView)findViewById(R.id.lv_PlayerListViewTitle);
		lv_PlayerListView.setOnItemClickListener(onItemClickListener);

		etxt_FirstName = (EditText)findViewById(R.id.etxt_FirstName);
		etxt_SecondName = (EditText)findViewById(R.id.etxt_SecondName);

		PSave.LoadPlayers(this);
		PopulateListView();

		lv_Stat = (ListView) findViewById(R.id.lv_Stat);
		lv_StatTitle = (ListView) findViewById(R.id.lv_StatTitle);

		lv_Break = (ListView) findViewById(R.id.lv_Break);
		lv_BreakTitle = (ListView) findViewById(R.id.lv_BreakTitle);

		host = (TabHost)findViewById(R.id.TabHost);
		host.setup();

		TabHost.TabSpec spec = host.newTabSpec("Tab One");
		spec.setContent(R.id.tab1);
		spec.setIndicator("Stats");
		host.addTab(spec);

		spec = host.newTabSpec("Tab Two");
		spec.setContent(R.id.tab2);
		spec.setIndicator("Breaks");
		host.addTab(spec);
	}

	private View.OnClickListener onClickListener = new View.OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			switch(v.getId())
			{
				case R.id.btn_Submit:
					String FirstName = etxt_FirstName.getText().toString();
					String SecondName = etxt_SecondName.getText().toString();
					String ID = String.valueOf(System.currentTimeMillis());

					if (FirstName.matches("[a-zA-Z]+") || SecondName.matches("[a-zA-Z]+"))
					{//if either name has letters in it
						Player NewPly = new Player(ID, FirstName, SecondName);
						PSave.SavePlayer(PlayerInfo.this, NewPly);

						PSave.LoadPlayers(PlayerInfo.this);
						PopulateListView();
					}
					else
					{// throw an error

					}

					break;
				case R.id.btn_Reset:
					PSave.DeletePlayerFile(PlayerInfo.this);

					PSave.LoadPlayers(PlayerInfo.this);
					PopulateListView();
					break;
			}
		}
	};

	private ListView.OnItemClickListener onItemClickListener = new ListView.OnItemClickListener()
	{
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id)
		{
			//Display player stats here
			TextView ID = (TextView)view.findViewById(R.id.ID);
			Player Ply = PSave.Players.get(ID.getText().toString());
			//dialog.CreateStatDialog(PlayerInfo.this, Ply);
			DisplayPlayerInfo(Ply);
		}
	};

	private void PopulateListView()
	{
		lv_PlayerListView.setAdapter(null);

		ListAdapter PlayerListAdapter, PlayerListTitleAdapter;

		ArrayList<HashMap<String, String>> PlayerlistArray = new ArrayList<>();
		ArrayList<HashMap<String, String>> PlayerListTitleArray = new ArrayList<>();

		//Sets up the title of the columns
		HashMap<String, String> PlayerListTitleMap = new HashMap<>();
		PlayerListTitleMap.put("ID", "ID");
		PlayerListTitleMap.put("first", "First Name");
		PlayerListTitleMap.put("second", "Second Name");
		PlayerListTitleArray.add(PlayerListTitleMap);

		//loop through vector
		for(Player Ply : PSave.Players.values())
		{
			HashMap<String, String> PlayerListMap = new HashMap<>();
			PlayerListMap.put("ID", String.valueOf(Ply.GetID()));
			PlayerListMap.put("first", Ply.GetFirstName());
			PlayerListMap.put("second", Ply.GetSecondName());
			PlayerlistArray.add(PlayerListMap);
		}

		try // Adds titles to listview
		{
			PlayerListTitleAdapter = new SimpleAdapter(this, PlayerListTitleArray, R.layout.player_info_row, new String[]{"ID","first","second"}, new int[]{R.id.ID, R.id.first, R.id.second});
			lv_PlayerListViewTitle.setAdapter(PlayerListTitleAdapter);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return;
		}

		try // Adds players to listview
		{
			PlayerListAdapter = new SimpleAdapter(this, PlayerlistArray, R.layout.player_info_row, new String[]{"ID","first","second"}, new int[]{R.id.ID, R.id.first, R.id.second});
			lv_PlayerListView.setAdapter(PlayerListAdapter);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return;
		}
	}

	private void DisplayPlayerInfo(Player Ply)
	{
		//tab 1 ////////////////////////////////////////////////////////////////////////////////////
		//Populate list view

		ListAdapter PlayerListAdapter, PlayerListTitleAdapter;

		lv_Stat.setAdapter(null);
		lv_StatTitle.setAdapter(null);

		ArrayList<HashMap<String, String>> PlayerlistArray = new ArrayList<>();
		ArrayList<HashMap<String, String>> PlayerListTitleArray = new ArrayList<>();

		//Sets up the title of the columns
		HashMap<String, String> PlayerListMap = new HashMap<>();
		PlayerListMap.put("StatName", "Stat Name");
		PlayerListMap.put("StatVal", "Stat Value");
		PlayerListTitleArray.add(PlayerListMap);

		try // Adds titles to listview
		{
			PlayerListTitleAdapter = new SimpleAdapter(this, PlayerListTitleArray, R.layout.player_stat_row, new String[]{"StatName","StatVal"}, new int[]{R.id.StatName, R.id.StatVal});
			lv_StatTitle.setAdapter(PlayerListTitleAdapter);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return;
		}

		// Treemap<String<Treemap<String,Integer>>
		for (TreeMap.Entry<String, TreeMap<String, Integer>> Entry : Ply.GetAllStats().entrySet()) // goes through and adds all stats
		{
			for (TreeMap.Entry<String, Integer> StatEntry : Entry.getValue().entrySet())
			{
				Integer Val = StatEntry.getValue();
				String Key = StatEntry.getKey();

				PlayerListMap = new HashMap<>();
				PlayerListMap.put("StatName", Key);
				PlayerListMap.put("StatVal", String.valueOf(Val));
				PlayerlistArray.add(PlayerListMap);
			}

			if (Entry.getKey().equals("General")) // adds shot accuracy
			{
				Integer ShotsMissed = Entry.getValue().get("Shots Missed");
				if (Entry.getValue().get("Shots Missed") == null)
				{
					ShotsMissed = 0;
				}

				if (Entry.getValue().get("Shots Taken") > 0)// prevents div 0 and nullpointer
				{
					PlayerListMap = new HashMap<>();
					PlayerListMap.put("StatName", "Shot Accuracy");
					PlayerListMap.put("StatVal", String.valueOf((double) (1 - ((double)ShotsMissed.intValue() / (double) Entry.getValue().get("Shots Taken"))) * 100) + "%");
					PlayerlistArray.add(PlayerListMap);
				}
			}

			PlayerListMap = new HashMap<>();
			PlayerListMap.put("StatName", "");
			PlayerListMap.put("StatVal", "");
			PlayerlistArray.add(PlayerListMap);
		}

		//

		try // Adds rows to listview
		{
			PlayerListAdapter = new SimpleAdapter(this, PlayerlistArray, R.layout.player_stat_row, new String[]{"StatName","StatVal"}, new int[]{R.id.StatName, R.id.StatVal});
			lv_Stat.setAdapter(PlayerListAdapter);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return;
		}

		//tab 2 ////////////////////////////////////////////////////////////////////////////////////
		//Populate list view

		lv_Break.setAdapter(null);
		lv_BreakTitle.setAdapter(null);

		PlayerlistArray = new ArrayList<>();
		PlayerListTitleArray = new ArrayList<>();

		//Sets up the title of the columns
		PlayerListMap = new HashMap<>();
		PlayerListMap.put("Position", "Position");
		PlayerListMap.put("Size", "Size of break");
		PlayerListMap.put("Date", "Date Completed");
		PlayerListTitleArray.add(PlayerListMap);

		try // Adds titles to listview
		{
			PlayerListTitleAdapter = new SimpleAdapter(this, PlayerListTitleArray, R.layout.player_info_row, new String[]{"Position","Size", "Date"}, new int[]{R.id.ID, R.id.first, R.id.second});
			lv_BreakTitle.setAdapter(PlayerListTitleAdapter);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return;
		}

		//
		for (int i = 0; i < Ply.GetBreakStats().size(); i++) // goes through and adds all stats
		{
			PlayerListMap = new HashMap<>();
			PlayerListMap.put("Position", String.valueOf(i));
			PlayerListMap.put("Size", String.valueOf(Ply.GetBreakStats().get(i).GetValue()));
			PlayerListMap.put("Date", String.valueOf(Ply.GetBreakStats().get(i).GetDateInSeconds())); // needs changing to a date no time
			PlayerlistArray.add(PlayerListMap);
		}
		//

		try // Adds rows to listview
		{
			PlayerListAdapter = new SimpleAdapter(this, PlayerlistArray, R.layout.player_info_row, new String[]{"Position","Size", "Date"}, new int[]{R.id.ID, R.id.first, R.id.second});
			lv_Break.setAdapter(PlayerListAdapter);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return;
		}
	}
}
